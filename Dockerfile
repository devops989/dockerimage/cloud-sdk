FROM alpine:latest
RUN apk add curl
RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
RUN chmod u+x kubectl
RUN mv kubectl /bin/kubectl
RUN apk add gettext
RUN apk add --update docker openrc
RUN rc-update add docker boot
RUN apk add python3
RUN apk add py3-pip
RUN pip install requests
